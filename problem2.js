const fs = require("fs").promises;

function readData() {
  fs.readFile("./lipsum.txt", "utf-8")
    .then((data) => {
      return fs.writeFile("./upper.txt", data.toUpperCase());
    })
    .then(() => {
      return fs.writeFile("./filenames.txt", "upper.txt\n");
    })
    .then(() => {
      return fs.readFile("./upper.txt", "utf-8");
    })
    .then((data) => {
      let lowerData = data.toLowerCase();
      let lowerAndSplitData = lowerData.split(".").join("\n");
      return fs.writeFile("./lowerAndSplit.txt", lowerAndSplitData);
    })
    .then(() => {
      return fs.appendFile("./filenames.txt", "lowerAndSplit.txt\n");
    })
    .then(() => {
      return fs.readFile("lowerAndSplit.txt", "utf-8");
    })
    .then((data) => {
      splitData = data.split("\n");
      let sortedData = splitData.sort().join("\n");
      return fs.writeFile("./sortedData.txt", sortedData);
    })
    .then(() => {
      return fs.appendFile("./filenames.txt", "sortedData.txt\n");
    })
    .then(() => {
      return fs.readFile("./filenames.txt", "utf-8");
    })
    .then((data) => {
      let deletePromiseArray = [];
      let filenamesArray = data.trim().split("\n");
      for (filename of filenamesArray) {
        deletePromiseArray.push(fs.unlink(`./${filename}`));
      }
      return Promise.all(deletePromiseArray);
    })
    .then(() => console.log("All files deleted"))
    .catch((error) => console.log(error));
}

// readData();
module.exports=readData;
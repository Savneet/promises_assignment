const fs = require("fs").promises;
const path = require("path");

const directoryPath = path.join(__dirname, "jsonFiles");

function createAndDeleteRandomFiles() {
  fs.mkdir(directoryPath)
    .then(() => {
      const numFiles = 5;
      const writePromises = [];
      // let count = 0;
      for (let index = 0; index < numFiles; index++) {
        let p = path.join(directoryPath, `file${index + 1}.json`);
        writePromises.push(
          fs.writeFile(p, JSON.stringify({ name: "Savneet", gender: "Female" }))
        );
      }

      Promise.all(writePromises);
      // if (count == numFiles) {
      //   //All the files are created, now we will delete them.
      //   for (let index = 0; index < numFiles; index++) {
      //     let p = path.join(directoryPath, `file${index + 1}.json`);
      //     fs.unlink(p);
      //   }
      // }
    })
    .then(() => {
      const numFiles = 5;
      const deletePromises = [];
      for (let index = 0; index < numFiles; index++) {
        let p = path.join(directoryPath, `file${index + 1}.json`);
        deletePromises.push(fs.unlink(p));
      }
      return Promise.all(deletePromises);
    })
    .then(() => console.log("All files deleted successfully"))
    .catch((error) => {
      console.log(error);
    });
}
module.exports=createAndDeleteRandomFiles;